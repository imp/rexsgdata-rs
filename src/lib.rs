//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

//! Scatter Gather Data Wrapper.

#![doc(html_root_url = "https://docs.rs/rexsgdata/0.12.0")]
#![cfg_attr(feature = "pedantic", warn(clippy::pedantic))]
#![warn(clippy::use_self)]
#![warn(deprecated_in_future)]
#![warn(future_incompatible)]
#![warn(unreachable_pub)]
#![warn(missing_debug_implementations)]
#![warn(rust_2018_compatibility)]
#![warn(rust_2018_idioms)]
#![warn(unused)]
#![deny(warnings)]
#![cfg_attr(feature = "nightly_features", feature(test))]
#[cfg(feature = "nightly_features")]
extern crate test;

pub use crate::iovec::Iovec;
pub use crate::sgdata::SgData;
pub use crate::sglist::SgList;

pub type RcData = SgData;

pub use crate::version::{version, VERSION};

mod iovec;
mod sgdata;
mod sglist;
mod utils;
mod version;
