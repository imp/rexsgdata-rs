//
// Copyright (c) 2019 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

/// Version of this crate (text)
pub const VERSION: &str = env!("CARGO_PKG_VERSION");

/// Version of this package (strongly typed)
pub fn version() -> semver::Version {
    VERSION.parse().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn valid_version() {
        let version = version();
        let major = env!("CARGO_PKG_VERSION_MAJOR").parse().unwrap();
        let minor = env!("CARGO_PKG_VERSION_MINOR").parse().unwrap();
        let patch = env!("CARGO_PKG_VERSION_PATCH").parse().unwrap();
        assert_eq!(version.major, major);
        assert_eq!(version.minor, minor);
        assert_eq!(version.patch, patch);
    }
}
