//
// Copyright (c) 2019 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::ops::Range;
use std::slice;

use libc::iovec;

pub(crate) unsafe fn iovec_as_slice(iov: &iovec) -> &[u8] {
    let base = iov.iov_base as *const u8;
    let len = iov.iov_len;
    slice::from_raw_parts(base, len)
}

pub(crate) unsafe fn iovec_as_static_slice(iov: iovec) -> &'static [u8] {
    let base = iov.iov_base as *const u8;
    let len = iov.iov_len;
    slice::from_raw_parts(base, len)
}

#[allow(clippy::mut_from_ref)]
pub(crate) unsafe fn iovec_as_slice_mut(iov: &iovec) -> &mut [u8] {
    let base = iov.iov_base as *mut u8;
    let len = iov.iov_len;
    slice::from_raw_parts_mut(base, len)
}

/// Returns a possible overlap between two ranges
pub(crate) fn intersection(r1: &Range<usize>, r2: &Range<usize>) -> Option<Range<usize>> {
    let r = r1.start.max(r2.start)..r1.end.min(r2.end);
    if r.is_empty() {
        None
    } else {
        Some(r)
    }
}
