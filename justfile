build:
    cargo build
clean:
    cargo clean
test:
    cargo test --all
update:
    cargo update
clippy:
    cargo clippy --all --tests
cbuild: clean build
ctest: clean test
bench:
    cargo bench --features nightly_features
pedantic:
    cargo clippy --all --tests --features pedantic
precommit:
    scripts/validate.sh
rustfmt:
    cargo fmt --all -- --check
fmt: rustfmt
check: fmt update test clippy
